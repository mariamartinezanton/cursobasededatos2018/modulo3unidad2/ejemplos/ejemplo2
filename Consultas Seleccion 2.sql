﻿--numero de ciclistas que hay--
SELECT COUNT(*) FROM ciclista;

--numero de ciclistas que hay en el equipo de Banesto--
SELECT COUNT(*) FROM ciclista WHERE nomequipo='banesto';

--edad media de los ciclistas--
SELECT AVG(edad) FROM ciclista;

--la edad media de los del equopo de banesto--
SELECT AVG(edad) FROM ciclista WHERE nomequipo='banesto' ;

--la edad media de los ciclistas de cada equipo--
SELECT nomequipo, AVG(edad) FROM ciclista GROUP BY nomequipo;

--numero de ciclistas por equipo--
SELECT COUNT(*),nomequipo FROM ciclista GROUP BY nomequipo;

--numero total de puertos--
SELECT COUNT(*) FROM puerto;

--numero total de puertos--
SELECT COUNT(*) FROM puerto where altura >1500;

--listar el nombre de los qeuipos que tengan mas de 4 ciclostas--
SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>4;

-- listar elnombre de los equipos que tengan mas de 4 ciclistas cuya edad este entre 28  Y 32--
SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*)>4;

-- indicame el numero de etapas que ha ganado cada uno de los ciclistas --
SELECT dorsal, COUNT(*) AS nEtapas FROM etapa GROUP BY dorsal;

-- indicame el dorsal de aquellos ciclistas que hayan ganado mas de 1 etapa --
  SELECT dorsal FROM etapa GROUP BY dorsal HAVING COUNT(*)>1;